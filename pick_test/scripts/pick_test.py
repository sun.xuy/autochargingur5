#! /usr/bin/env python
import sys
import rospy
import moveit_commander
import geometry_msgs.msg

import argparse
import actionlib
import os


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('pick_up',anonymous = True)
robot = moveit_commander.RobotCommander()
arm_group = moveit_commander.MoveGroupCommander('manipulator')

reference_frame = 'base_link'
arm_group.set_pose_reference_frame(reference_frame)


# hand_group = moveit_commander.MoveGroupCommander('endeffector')
#  # open the gripper
# hand_group.set_named_target('open')
# plan2 = hand_group.go()
# # put the arm at the 1st grasping position


# go to an initial starting position
joint_positions = [0.391410, -0.676384, -0.376217, 0.0, 1.052834, 0.454125]
# joint_positions = [0.0, -0.5, -0.0, 0.0, 0.0, 0.0]
arm_group.set_joint_value_target(joint_positions)
plan1 = arm_group.go()

os.system("rosrun ur5_gazebo send_gripper.py --value 0.0")


pose_target = geometry_msgs.msg.Pose()

pose_target.orientation.w = 0.5
pose_target.orientation.x = -0.5
pose_target.orientation.y = 0.5
pose_target.orientation.x = -0.5

pose_target.position.x = 0.4
pose_target.position.y = -0.2
pose_target.position.z = 0.3


arm_group.set_start_state_to_current_state()
arm_group.set_pose_target(pose_target)

_, traj, _, _ = arm_group.plan()
# traj= arm_group.plan()
plan1 = arm_group.execute(traj)

# joint_positions = [0.391410, -0.676384, -0.376217, 0.0, 1.052834, 0.454125]
# arm_group.set_joint_value_target(joint_positions)
# plan1 = arm_group.go()





# put the arm at the 2nd grasping position
pose_target.position.z = 0.7
arm_group.set_pose_target(pose_target)
_, traj, _, _ = arm_group.plan()
# traj= arm_group.plan()
plan1 = arm_group.execute(traj)


os.system("rosrun ur5_gazebo send_gripper.py --value 0.8")

pose_target.position.z = 0.75
arm_group.set_pose_target(pose_target)
_, traj, _, _ = arm_group.plan()
# traj= arm_group.plan()
plan1 = arm_group.execute(traj)




rospy.sleep(5)
moveit_commander.roscpp_shutdown()



#################################

