#! /usr/bin/env python
import sys
import rospy
import moveit_commander
import geometry_msgs.msg
from squaternion import euler2quat, quat2euler, Quaternion

import argparse
import actionlib
import os


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('pick_up',anonymous = True)
robot = moveit_commander.RobotCommander()
arm_group = moveit_commander.MoveGroupCommander('manipulator')
gripper = moveit_commander.MoveGroupCommander('robotiq_gripper')
reference_frame = 'base_link'
arm_group.set_pose_reference_frame(reference_frame)
end_effector_link = arm_group.get_end_effector_link()

pose_target = geometry_msgs.msg.Pose()

Yaw = 1.590050
Pitch = -0.000512
Roll = 0.000040

q = euler2quat(Yaw, Pitch, Roll , degrees=False)

pose_target.orientation.x = q[0]
pose_target.orientation.y = q[1]
pose_target.orientation.z = q[2]
pose_target.orientation.w = q[3]

pose_target.position.x = -0.07
pose_target.position.y = 0.52
pose_target.position.z = 0.73

arm_group.set_start_state_to_current_state()
arm_group.set_pose_target(pose_target,end_effector_link)

#_, traj, _, _ = arm_group.plan()
traj= arm_group.plan()
plan1 = arm_group.execute(traj)




rospy.sleep(1)
moveit_commander.roscpp_shutdown()


