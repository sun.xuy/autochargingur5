#! /usr/bin/env python
import sys
import rospy
import moveit_commander
import geometry_msgs.msg

import argparse
import actionlib
import os


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('pick_up',anonymous = True)
robot = moveit_commander.RobotCommander()
arm_group = moveit_commander.MoveGroupCommander('manipulator')

reference_frame = 'base_link'
arm_group.set_pose_reference_frame(reference_frame)


# hand_group = moveit_commander.MoveGroupCommander('endeffector')
#  # open the gripper
# hand_group.set_named_target('open')
# plan2 = hand_group.go()
# # put the arm at the 1st grasping position


# go to an initial starting position
joint_positions = [0.4, -0.7, -0.4, 0.0, 1.05, 0.45]
# joint_positions = [0.0, -0.5, -0.0, 0.0, 0.0, 0.0]
arm_group.set_joint_value_target(joint_positions)
plan1 = arm_group.go()




rospy.sleep(1)
moveit_commander.roscpp_shutdown()



#################################

