# autoChargingUr5

##Dependency
Gazebo_pkg

git clone https://github.com/JenniferBuehler/gazebo-pkgs

sudo apt-get install ros-melodic-object-recognition-msgs

git clone https://github.com/JenniferBuehler/general-message-pkgs.git

## Installation
Go to your workspace/src
```bash
git clone https://gitlab.com/sun.xuy/autochargingur5
cd ..
catkin_make
source devel/setup.bash
```
Install the ROS controllers
```bash
sudo apt-get install ros*controller*
```

## Starting Gazebo Simulation

The simulation is launched by:
```bash
$ roslaunch ur5_gazebo ur5_cubes.launch
```
or 
```bash
$ roslaunch ur5_gazebo ur5_setup.launch
```
The Gazebo simulation starts in paused state.
Before starting up MoveIt unpause the Gazebo simulation.

## Starting MoveIt control


Launch the MoveIt control by:
```bash
$ roslaunch robotiq_ur5 robotiq_ur5_planning_execution.launch
```
You now should see the same posture in the Gazebo as the MoveIt environment.
Set in Rviz the `MotionPlanning/Planning Request/Planning Group` to manipulator to drag the arm around. 
By `Plan and Execute` the arm will move to its new position.


## Control your gripper robotiq85
By default the simulation starts paused. Unpause the simulation. You can then send commands to the
joints or to the gripper.

The following is an example of an action client to change the gripper configuration. Open a new
terminal, and then execute:
  ```
  $ rosrun ur5_gazebo send_gripper.py --value 0.5
  ```
where the value is a float between 0.0 (open) and 0.8 (closed).

An example of sending joints values to the robot can be executed as follows:
  ```
  $ rosrun ur5_gazebo send_joints.py
  ```
To change the values of the joints, the file `send_joints.py` must be modified.


## Control your ur5 with robotiq gripper85, test run

The simulation is launched by:
```bash
$ rosrun pick_test pick_test.py
```






